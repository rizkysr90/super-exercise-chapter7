'use strict';
const bcrypt = require('bcrypt');
const usersData = require('../masterdata/users.json').map((eachData) => {
  eachData.createdAt = new Date();
  eachData.updatedAt = new Date();
  
  eachData.password =  bcrypt.hashSync(eachData.password, +process.env.SALT_HASH)
  return eachData;
})
module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
     await queryInterface.bulkInsert('Users',usersData,{}); 
   
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('Users',null,{truncate: true, restartIdentity: true});
  }
};
