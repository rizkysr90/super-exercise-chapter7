const router = require('express').Router();
const jwt = require('../middlewares/passport');
const authRoles = require('../middlewares/verifyRole');
const brandValidator = require('../validators/brands.validator');
const brandController = require('../controllers/brands.controller');

router.post('/',jwt,authRoles.roleValidator(['Member']),authRoles.verifyRole,brandValidator.create(),brandValidator.validate,brandController.create);
router.route('/:brand_id')
    .get(jwt,brandValidator.idParam(),brandValidator.validate,brandController.getById)
    .put(jwt,authRoles.roleValidator(['Member']),authRoles.verifyRole,brandValidator.idParam(),brandValidator.create(),brandValidator.validate,brandController.update)
    .delete(jwt,authRoles.roleValidator(['Member']),authRoles.verifyRole,brandValidator.idParam(),brandValidator.validate,brandController.remove)

module.exports = router;