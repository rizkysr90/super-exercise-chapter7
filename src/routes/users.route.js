const router = require('express').Router();
const jwt = require('../middlewares/passport');
const authRoles = require('../middlewares/verifyRole');
const userController = require('../controllers/users.controller');
const userValidator = require('../validators/users.validator');

router.route('/')
    .post(jwt,authRoles.roleValidator(['Superadmin','Admin']),authRoles.verifyRole,userValidator.create(),userValidator.validate,userController.create);
router.post('/login',userValidator.login(),userValidator.validate,userController.login);
router.route('/:user_id')
    .put(jwt,authRoles.roleValidator(['Superadmin']),authRoles.verifyRole,userValidator.idParam(),userValidator.update(),userValidator.validate,userController.update)
    .get(jwt,userValidator.idParam(),userValidator.validate,userController.getById)
    .delete(jwt,authRoles.roleValidator(['Superadmin']),authRoles.verifyRole,userValidator.idParam(),userValidator.validate,userController.remove)
// router.route('/')
//     .post()

module.exports = router;