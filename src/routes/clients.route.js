const router = require('express').Router();
const jwt = require('../middlewares/passport');
const authRoles = require('../middlewares/verifyRole');
const clientValidator = require('../validators/clients.validator');
const clientController = require('../controllers/clients.controller');

router.post('/',jwt,authRoles.roleValidator(['Member']),authRoles.verifyRole,clientValidator.create(),clientValidator.validate,clientController.create);
router.route('/:client_id')
    .get(jwt,clientValidator.idParam(),clientValidator.validate,clientController.getById)
    .put(jwt,authRoles.roleValidator(['Member']),authRoles.verifyRole,clientValidator.idParam(),clientValidator.create(),clientValidator.validate,clientController.update)
    .delete(jwt,authRoles.roleValidator(['Member']),authRoles.verifyRole,clientValidator.idParam(),clientValidator.validate,clientController.remove)

module.exports = router;