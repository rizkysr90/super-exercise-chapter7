const express = require('express');
const router = express.Router();
const userRoute = require('./users.route');
const clientRoute = require('./clients.route');
const brandRoute = require('./brands.route');

router.get('/',(req,res,next) => res.send('Hello World'));
router.use('/users/',userRoute);
router.use('/clients/',clientRoute);
router.use('/brands',brandRoute);

module.exports = router;