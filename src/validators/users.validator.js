const { body, param, query, validationResult } = require('express-validator');
const ApiError = require('../models/errorResponse.model');
const constant = require('../constant/constant');

const login = () => {
    return [
        body(['email','password'],constant.IS_EMPTY_MESSAGE).notEmpty(),
        body('email',constant.IS_INVALID_EMAIL).isEmail()
    ]
}
const create = () => {
    return [
      body(['email','password','role_id'],constant.IS_EMPTY_MESSAGE).notEmpty(),
      body('email',constant.IS_INVALID_EMAIL).isEmail(),
      body('role_id').toInt()
    ]
}
const update = () => {
    return [
      body(['email','role_id'],constant.IS_EMPTY_MESSAGE).notEmpty(),
      body('email',constant.IS_INVALID_EMAIL).isEmail(),
      body('role_id').toInt()
    ]
}
const idParam = () => {
    return [
      param('user_id',constant.IS_NUMBER_MESSAGE).isInt()
    ]
  }
  
const validate = (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
          if (errors.errors[0].msg === constant.IS_EMPTY_MESSAGE ||
              errors.errors[0].msg === constant.IS_NUMBER_MESSAGE ||
              errors.errors[0].msg === constant.IS_INVALID_EMAIL) {
              throw new ApiError.BadRequest({errors : errors.array()});
          }
      }
      next();
    } catch (error) {
        next(error)
    }
  }
  

module.exports = {
    login,
    validate,
    idParam,
    create,
    update
}
