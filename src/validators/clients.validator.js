const { body, param, query, validationResult } = require('express-validator');
const ApiError = require('../models/errorResponse.model');
const constant = require('../constant/constant');


const create = () => {
    return [
      body(['name','phone'],constant.IS_EMPTY_MESSAGE).notEmpty(),
      body('phone').isNumeric()
    ]
}

const idParam = () => {
    return [
      param('client_id',constant.IS_NUMBER_MESSAGE).isInt()
    ]
  }
  
const validate = (req, res, next) => {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
          if (errors.errors[0].msg === constant.IS_EMPTY_MESSAGE ||
              errors.errors[0].msg === constant.IS_NUMBER_MESSAGE ||
              errors.errors[0].msg === constant.IS_INVALID_EMAIL) {
              throw new ApiError.BadRequest({errors : errors.array()});
          }
      }
      next();
    } catch (error) {
        next(error)
    }
  }
  

module.exports = {
    validate,
    idParam,
    create,
}
