const BaseResponse = require("../models/baseResponse.model");
const brandService = require("../services/brands.service");


module.exports = {
    async getById(req,res,next) {
        try {
            const result = await brandService.getById(req.params.brand_id);
            res.status(200).json(BaseResponse.statusOK(result));

        } catch (error) {
            next(error)
        }
    },
    async update(req,res,next) {
        try {
            const result = await brandService.update(req.params.brand_id,req.body);
            res.status(200).json(BaseResponse.statusOK(result));
        } catch (error) {
            next(error)
        }
    },
    async remove(req,res,next) {
        try {
           const result = await brandService.remove(req.params.brand_id);
           res.status(200).json(BaseResponse.statusOK(result));
        } catch (error) {
           next(error);
        }
    },
    async create(req,res,next){
        try {
           const result = await brandService.create(req.body);
           res.status(201).json(BaseResponse.created(result));
        } catch(error) {
           next(error);
        }
     }
}