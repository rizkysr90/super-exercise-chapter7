const BaseResponse = require("../models/baseResponse.model");
const clientService = require("../services/clients.service");


module.exports = {
    async getById(req,res,next) {
        try {
            const result = await clientService.getById(req.params.client_id);
            res.status(200).json(BaseResponse.statusOK(result));

        } catch (error) {
            next(error)
        }
    },
    async update(req,res,next) {
        try {
            const result = await clientService.update(req.params.client_id,req.body);
            res.status(200).json(BaseResponse.statusOK(result));
        } catch (error) {
            next(error)
        }
    },
    async remove(req,res,next) {
        try {
           const result = await clientService.remove(req.params.client_id);
           res.status(200).json(BaseResponse.statusOK(result));
        } catch (error) {
           next(error);
        }
    },
    async create(req,res,next){
        try {
           const result = await clientService.create(req.body);
           res.status(201).json(BaseResponse.created(result));
        } catch(error) {
           next(error);
        }
     }
}