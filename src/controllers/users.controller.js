const BaseResponse = require("../models/baseResponse.model");
const userService = require("../services/users.service");

module.exports = {
   async login(req,res,next) {
      try {
         const result = await userService.login(req.body);
         res.status(200).json(BaseResponse.statusOK(result));
      } catch (error) {
         next(error);
      }
   },
   async getById(req,res,next) {
      try {
         const result = await userService.getById(req.params.user_id);
         res.status(200).json(BaseResponse.statusOK(result));

      } catch (error) {
         next(error);
      }
   },
   async create(req,res,next){
      try {
         const result = await userService.create(req.body);
         res.status(201).json(BaseResponse.created(result));
      } catch(error) {
         next(error);
      }
   },
   async update(req,res,next) {
      try {
         const result = await userService.update(req.params.user_id,req.body);
         res.status(200).json(BaseResponse.statusOK(result));
      } catch (error) {
         next(error);
      }
   },
   async remove(req,res,next) {
      try {
         const result = await userService.remove(req.params.user_id);
         res.status(200).json(BaseResponse.statusOK(result));
      } catch (error) {
         next(error);
      }
   }
}