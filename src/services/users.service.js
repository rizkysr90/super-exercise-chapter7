const {User,Role} = require('../../models/');
const ErrorResponse =require('../models/errorResponse.model');
const {verifyPassword,issueJWT} = require('../libs/utils');
const {Op} = require("sequelize");


module.exports = {
    async login(reqBody) {
        const {email,password} = reqBody;
        const optionsFindUser = {
            where : {
                "email" : email
            }
        }
        const foundUser = await User.findOne(optionsFindUser);
        if(!foundUser) {
            throw new ErrorResponse.NotFound({errors: 'User not found'});
        }
        const isValidPassword = await verifyPassword(password,foundUser.password);
        if (!isValidPassword) {
            throw new ErrorResponse.BadRequest({errors: 'Wrong password'});
        }
        return issueJWT(foundUser);
    },
    async getById(user_id) {
        const optionsFindUser = {
            attributes: {exclude: ['password','createdAt','updatedAt','role_id']},
            where : {
                "id" : user_id
            },
            include : Role,
            
        }
        const foundUser = await User.findOne(optionsFindUser);
        if(!foundUser) {
            throw new ErrorResponse.NotFound({errors : 'User not found'})
        }
        return foundUser;
    },
    async create(reqBody) {
        const optionsFindUser = {
            where : {
                "email" : reqBody.email
            }
        }
        const foundUser = await User.findOne(optionsFindUser);
        if (foundUser) {
            throw new ErrorResponse.BadRequest({errors : 'Email was used'});
        }
        const createdUser = await User.create(reqBody);
        return createdUser.id;
    },
    async update(user_id,reqBody) {
        const optionsFindUniqueEmail = {
            where : {
                "email" : reqBody.email,
                "id" : {
                    [Op.not] : user_id
                }
            }
        }
        const checkUniqeEmail = await User.findOne(optionsFindUniqueEmail);
        if(checkUniqeEmail) {
            throw new ErrorResponse.BadRequest({errors : `User email  must be unique, email ${reqBody.email} was used`});
        }
        const optionsUpdate = {
            where : {
                id : user_id
            }
        }
        const result =  await User.update(reqBody,optionsUpdate);
        if (result[0] === 0) {
            throw new ErrorResponse.NotFound({errors : `User not found`});
        }
        return result;
    },
    async remove(user_id) {
        const result = await User.destroy({where : {
            id : user_id
        }});
        if(result === 0){
            throw new ErrorResponse.NotFound({errors : `User not found`})
        }
        return result;
    
    }
}