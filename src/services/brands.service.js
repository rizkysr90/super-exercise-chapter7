const {Brand,Client} = require('../../models/');
const ErrorResponse =require('../models/errorResponse.model');

module.exports = {
    async getById(brand_id) {
        const optionsFindUser = {
            attributes: {exclude: ['createdAt','updatedAt','client_id']},
            where : {
                "id" : brand_id
            },
            include : Client
            
        }
        const foundUser = await Brand.findOne(optionsFindUser);
        if(!foundUser) {
            throw new ErrorResponse.NotFound({errors : 'Brand not found'})
        }
        return foundUser;
    },
    async update(brand_id,reqBody) {
        const optionsUpdate = {
            where : {
                id : brand_id
            }
        }
        const result =  await Brand.update(reqBody,optionsUpdate);
        if (result[0] === 0) {
            throw new ErrorResponse.NotFound({errors : `Brand not found`});
        }
        return result;
    },
    async remove(brand_id) {
        const result = await Brand.destroy({where : {
            id : brand_id
        }});
        if(result === 0){
            throw new ErrorResponse.NotFound({errors : `Brand not found`})
        }
        return result;
    
    },
    async create(reqBody) {
        const createdUser = await Brand.create(reqBody);
        return createdUser.id;
    }
}