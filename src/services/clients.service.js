const {Client} = require('../../models/');
const ErrorResponse =require('../models/errorResponse.model');

module.exports = {
    async getById(client_id) {
        const optionsFindUser = {
            attributes: {exclude: ['createdAt','updatedAt']},
            where : {
                "id" : client_id
            },
            
        }
        const foundUser = await Client.findOne(optionsFindUser);
        if(!foundUser) {
            throw new ErrorResponse.NotFound({errors : 'Client not found'})
        }
        return foundUser;
    },
    async update(client_id,reqBody) {
        const optionsUpdate = {
            where : {
                id : client_id
            }
        }
        const result =  await Client.update(reqBody,optionsUpdate);
        if (result[0] === 0) {
            throw new ErrorResponse.NotFound({errors : `Client not found`});
        }
        return result;
    },
    async remove(client_id) {
        const result = await Client.destroy({where : {
            id : client_id
        }});
        if(result === 0){
            throw new ErrorResponse.NotFound({errors : `Client not found`})
        }
        return result;
    
    },
    async create(reqBody) {
        const createdUser = await Client.create(reqBody);
        return createdUser.id;
    }
}