const ErrorResponse = require('../models/errorResponse.model');

function verifyRole(req,res,next) {
    const role_id = Number(req.user.role_id);
    const isValidRole = req.onlyRoles.includes(role_id);
    if(isValidRole) {
        next();
    } else {
        throw new ErrorResponse.Unauthorized({errors: `You don't have access to this server`});
    }
}


function roleValidator(arr){
    const replicateDbRole = {
        'Superadmin' : 1,
        'Admin': 2,
        'Member': 3
    }
    const data = [];
    arr.forEach((elm) => {
        if (elm === 'Superadmin') {
            data.push(replicateDbRole.Superadmin);
        } else if (elm === 'Admin') {
            data.push(replicateDbRole.Admin);
        } else if (elm === 'Member') {
            data.push(replicateDbRole.Member);
        } else {
            throw new Error('Invalid argumen for role validator middleware');
        }
    })
    const myMiddleware = function(req,res,next) {
        req.onlyRoles = data;
        next();
    }
    return myMiddleware;
}

module.exports = {
    verifyRole,
    roleValidator
}