module.exports = {
    IS_EMPTY_MESSAGE : 'Field value cannot be an empty string',
    IS_NUMBER_MESSAGE : 'Field value must be an number',
    IS_INVALID_EMAIL : 'Email value is invalid'
}
