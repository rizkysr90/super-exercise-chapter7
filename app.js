require('dotenv').config();
const express = require('express');
const app = express();
const morgan = require('morgan');
const routes = require('./src/routes/');

app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({extended : true}));
app.use('/',routes);
app.all('*',(req,res) => {
    res.status(404).json('Page Not Found');
});


/* Error handler middleware */
app.use((err, req, res, next) => {
    console.log(err);
    const statusCode = err.statusCode || 500;
    const statusMessage = err.message || 'Internal Server Error';
    res.status(statusCode).json({
        error : {
            message : statusMessage,
            code : statusCode,
            errors : err.errors
        }
    });
    
});
module.exports = app