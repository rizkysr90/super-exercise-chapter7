# HERE COMES ANOTHER CHALLENGE OF AUTHENTICATION & AUTHORIZATION

## Directions

- Anda diminta membuat 4 tabel, yaitu `Clients`, `Brands`, `Users`, dan `Roles`.  
    - Tugas Anda diminta untuk membuat CRUD yang membuat, membaca, mengedit, dan menghapus `3` dari `4` table tersebut,
      yaitu `Clients`, `Brands` dan `Users`.

- Untuk tabel `Roles`: 
    - Buat dia statis dan predefined (dapat menggunakan master data), dan tidak perlu dibuatkan CRUD.
    - `Role` yang perlu dibuat adalah: `Superadmin`, `Admin`, dan `Member`.
    - Dapat memiliki banyak `Users`. 

- Untuk tabel `Users`:
    - Attribute didalamnya bebas (wajib ada email dan password), yang penting memiliki satu `Roles`.
    - Proses `READ` dapat dilakukan semua role.
    - Proses `CREATE` hanya dapat dilakukan oleh User dengan role `Superadmin` dan `Admin`.
    - PProses `UD` hanya dapat dilakukan oleh User dengan role `Superadmin`.

- Untuk tabel `Clients`: 
    - Attribut didalamnya bebas, yang penting dia memiliki banyak `Brands`.
    - Proses `READ` dapat dilakukan semua role.
    - Proses `CUD` hanya dapat dilakukan oleh User dengan role `Member`.

- Untuk tabel `Brands`: 
    - Attribut didalamnya bebas, hanya dapat memiliki satu `Clients`.
    - Proses `READ` dapat dilakukan semua role.
    - Proses `CUD` hanya dapat dilakukan oleh User dengan role `Member`.


- Gunakan middleware `morgan` untuk menampilkan logging message.

# RESTRICTIONS

- Gunakanlah token based auth untuk challenge ini.
- Bebas gunakan metode tokenisasi yang ada diluar sana, password wajib di enkripsi menggunakan `bcrypt`.