'use strict';

const {
  Model
} = require('sequelize');
const bcrypt = require('bcrypt');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      const options = {
        foreignKey: 'role_id',
      }
      models.Role.hasMany(models.User,options);
      models.User.belongsTo(models.Role,options);
    }
  }
  User.init({
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    role_id: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'User',
    hooks: {
      beforeCreate: async (User, options) => {
        User.password = await bcrypt.hash(User.password, +process.env.SALT_HASH);
        return User;
      }
    }

  });
  return User;
};